
var socket = io();

socket.on("connect", function(){ // "connect" es una palabra reservada
    console.log("conectado al servidor");
});

socket.on("disconnect", function(){ // "disconnect" es una palabra reservada
    console.log("Perdimos conexión con el servidor");
});

enviarDataHaciaServidor(socket)

recibirDataDesdeServidor(socket)





// FUNCIONES:________________________________________________

function enviarDataHaciaServidor(socket){
    socket.emit(
        "enviarMensaje",
        {
            usuario: "Gonzalo",
            mensaje: "Hola mundo",
        }
    );
}

function recibirDataDesdeServidor(socket){
    // escuchar información
    socket.on("enviarMensaje", function(mensaje){
        console.log("servidor mensaje:", mensaje);
    })
}